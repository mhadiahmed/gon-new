

<?php $__env->startSection('content'); ?>
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-4">
                <h1 class="h3"><?php echo e(translate('All products')); ?></h1>
            </div>
            <div class="col-md-8 text-md-right">
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_products')): ?>
                    <a href="<?php echo e(route('product.create')); ?>" class="btn btn-primary">
                        <span><?php echo e(translate('Add New Product')); ?></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="card">
        <form class="" id="sort_products" action="" method="GET">
            <div class="card-header row gutters-5">
                <div class="col text-center text-md-left">
                    <h5 class="mb-md-0 h6"><?php echo e(translate('All Products')); ?></h5>
                </div>
                <div class="col-md-2 ml-auto">
                    <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" name="type" id="type"
                        onchange="sort_products()">
                        <option value=""><?php echo e(translate('Sort By')); ?></option>
                        <option value="rating,desc"
                        <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'rating' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>>
                        <?php echo e(translate('Rating (High > Low)')); ?></option>
                    <option value="rating,asc"
                    <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'rating' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>>
                    <?php echo e(translate('Rating (Low > High)')); ?></option>
                <option value="num_of_sale,desc"
                <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'num_of_sale' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>>
                <?php echo e(translate('Num of Sale (High > Low)')); ?></option>
            <option value="num_of_sale,asc"
            <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'num_of_sale' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>>
            <?php echo e(translate('Num of Sale (Low > High)')); ?></option>
        <option value="unit_price,desc"
        <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'unit_price' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>>
        <?php echo e(translate('Base Price (High > Low)')); ?></option>
    <option value="unit_price,asc"
        <?php if(isset($col_name, $query)): ?> <?php if($col_name == 'unit_price' && $query == 'asc'): ?> selected <?php endif; ?>
    <?php endif; ?>>
    <?php echo e(translate('Base Price (Low > High)')); ?></option>
</select>
</div>
<div class="col-md-2">
<div class="input-group">
<input type="text" class="form-control form-control-sm" id="search" name="search"
    <?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?>
    placeholder="<?php echo e(translate('Type & Enter')); ?>">
</div>
</div>
</div>
</form>
<div class="card-body">
<table class="table aiz-table mb-0">
<thead>
<tr>
<th class="w-40px">#</th>
<th class="col-xl-2"><?php echo e(translate('Name')); ?></th>
<th data-breakpoints="md"><?php echo e(translate('Info')); ?></th>
<th data-breakpoints="md" width="20%"><?php echo e(translate('Categories')); ?></th>
<th data-breakpoints="md"><?php echo e(translate('Brand')); ?></th>
<th data-breakpoints="md"><?php echo e(translate('Published')); ?></th>
<th data-breakpoints="md" class="text-right"><?php echo e(translate('Options')); ?></th>
</tr>
</thead>
<tbody>
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <td><?php echo e($key + 1 + ($products->currentPage() - 1) * $products->perPage()); ?></td>
    <td>
        <a href="<?php echo e(route('product', $product->slug)); ?>" target="_blank"
            class="text-reset d-block">
            <div class="d-flex align-items-center">
                <img src="<?php echo e(uploaded_asset($product->thumbnail_img)); ?>" alt="Image"
                    class="size-60px size-xxl-80px mr-2"
                    onerror="this.onerror=null;this.src='<?php echo e(static_asset('/assets/img/placeholder.jpg')); ?>';" />
                <span class="flex-grow-1 minw-0">
                    <div class=" text-truncate-2 fs-12">
                        <?php echo e($product->getTranslation('name')); ?></div>
                </span>
            </div>
        </a>
    </td>
    <td>
        <div>
            <div><span><?php echo e(translate('Rating')); ?></span>: <span
                    class="rating rating-sm my-2"><?php echo e(renderStarRating($product->rating)); ?></span>
            </div>
            <div><span><?php echo e(translate('Toal Sold')); ?></span>: <span
                    class="fw-600"><?php echo e($product->num_of_sale); ?></span></div>
            <div>
                <span><?php echo e(translate('Price')); ?></span>:
                <?php if($product->highest_price != $product->lowest_price): ?>
                    <span class="fw-600"><?php echo e(format_price($product->lowest_price)); ?> -
                        <?php echo e(format_price($product->highest_price)); ?></span>
                <?php else: ?>
                    <span
                        class="fw-600"><?php echo e(format_price($product->lowest_price)); ?></span>
                <?php endif; ?>
            </div>
        </div>
    </td>
    <td>
        <?php $__currentLoopData = $product->categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <span
                class="badge badge-inline badge-md bg-soft-dark mb-1"><?php echo e($category->getTranslation('name')); ?></span>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </td>
    <td>
        <?php if($product->brand): ?>
            <div class="h-50px w-100px d-flex align-items-center justify-content-center">
                <img src="<?php echo e(uploaded_asset($product->brand->logo)); ?>"
                    alt="<?php echo e(translate('Brand')); ?>" class="mw-100 mh-100"
                    onerror="this.onerror=null;this.src='<?php echo e(static_asset('/assets/img/placeholder.jpg')); ?>';" />
            </div>
        <?php else: ?>
            <span><?php echo e(translate('No brand')); ?></span>
        <?php endif; ?>
    </td>
    <td>
        <label class="aiz-switch aiz-switch-success mb-0">
            <input onchange="update_published(this)" value="<?php echo e($product->id); ?>" type="checkbox"
                <?php if($product->published == 1): ?> checked <?php endif; ?>>
            <span class="slider round"></span>
        </label>
    </td>
    <td class="text-right">
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_products')): ?>
            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm"
                href="<?php echo e(route('product.show', $product->id)); ?>" title="<?php echo e(translate('View')); ?>">
                <i class="las la-eye"></i>
            </a>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_products')): ?>
            <a class="btn btn-soft-info btn-icon btn-circle btn-sm"
                href="<?php echo e(route('product.edit', ['id' => $product->id, 'lang' => env('DEFAULT_LANGUAGE')])); ?>"
                title="<?php echo e(translate('Edit')); ?>">
                <i class="las la-edit"></i>
            </a>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('duplicate_products')): ?>
            <a class="btn btn-soft-success btn-icon btn-circle btn-sm"
                href="<?php echo e(route('product.duplicate', ['id' => $product->id, 'type' => $type])); ?>"
                title="<?php echo e(translate('Duplicate')); ?>">
                <i class="las la-copy"></i>
            </a>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete_products')): ?>
            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete"
                data-href="<?php echo e(route('product.destroy', $product->id)); ?>"
                title="<?php echo e(translate('Delete')); ?>">
                <i class="las la-trash"></i>
            </a>
        <?php endif; ?>
    </td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>
<div class="aiz-pagination">
<?php echo e($products->appends(request()->input())->links()); ?>

</div>
</div>
</div>
<?php
//CoreComponentRepository::instantiateShopRepository();
?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('modal'); ?>
<?php echo $__env->make('backend.inc.delete_modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
    });

    function update_published(el) {
        if (el.checked) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.post('<?php echo e(route('product.published')); ?>', {
            _token: '<?php echo e(csrf_token()); ?>',
            id: el.value,
            status: status
        }, function(data) {
            if (data == 1) {
                AIZ.plugins.notify('success', '<?php echo e(translate('Published products updated successfully')); ?>');
            } else {
                AIZ.plugins.notify('danger', '<?php echo e(translate('Something went wrong')); ?>');
            }
        });
    }

    function sort_products(el) {
        $('#sort_products').submit();
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\shohan\shop\upload_this\resources\views/backend/product/products/index.blade.php ENDPATH**/ ?>
